package tricksandfeatures;

//Дано:   a = true, b = true, c = false. Какой результат будет у этого выражения: (a | b && c) ? (Пожалуйста не пользуйтесь IDE)
public class LogicalOperators {
    public static void main(String[] args) {
        boolean a = true, b = true, c = false;
        System.out.println(a | b && c);
    }
}
